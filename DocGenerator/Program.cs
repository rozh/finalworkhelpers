﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocGenerator.Model;
using ExcelDataReader;
using TemplateEngine.Docx;

namespace DocGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var filePath = args[0];
            var students = new List<Student>();
            var cometee = new List<string>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    do
                    {
                        while (reader.Read())
                        {
                            if (reader.Name == "students")
                            {
                                var student = new Student();
                                var i = 0;
                                student.Group = Convert.ToString(reader.GetValue(i++));
                                student.Name = reader.GetString(i++);
                                student.NameRP = reader.GetString(i++);
                                student.StudyDirection = reader.GetString(i++);
                                student.StudyProfile = reader.GetString(i++);
                                student.Programm = reader.GetString(i++);

                                students.Add(student);
                            }
                            else
                            {
                                cometee.Add(Convert.ToString(reader.GetValue(0)));
                            }
                        }
                    } while (reader.NextResult());

                }
            }

            foreach (var student in students)
            {
                var fileName = $"result\\compet_{student.Group}_{student.Name}.docx";
                Console.Write(fileName);
                if (!Directory.Exists("result")) Directory.CreateDirectory("result");
                File.Copy("Templates\\compet.docx", fileName, true);

                var valuesToFill = new Content(
                    new FieldContent("programm", student.Programm),
                    new FieldContent("direction", student.StudyDirection),
                    new FieldContent("profile", student.StudyProfile),
                    new FieldContent("student", student.NameRP),
                    new FieldContent("general", cometee.First()));

                using (var outputDocument = new TemplateProcessor(fileName)
                    .SetRemoveContentControls(true))
                {
                    outputDocument.FillContent(valuesToFill);
                    outputDocument.SaveChanges();
                }
                Console.WriteLine(" ... done");
            }

            foreach (var student in students)
            {
                var fileName = $"result\\score_{student.Group}_{student.Name}.docx";
                Console.Write(fileName);
                if (!Directory.Exists("result")) Directory.CreateDirectory("result");
                File.Copy("Templates\\score.docx", fileName, true);

                var valuesToFill = new Content(
                    new FieldContent("programm", student.Programm),
                    new FieldContent("direction", student.StudyDirection),
                    new FieldContent("student", student.NameRP),
                    new FieldContent("general", cometee.First()));

                using (var outputDocument = new TemplateProcessor(fileName)
                    .SetRemoveContentControls(true))
                {
                    outputDocument.FillContent(valuesToFill);
                    outputDocument.SaveChanges();
                }
                Console.WriteLine(" ... done");
            }
        }
    }
}
