﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocGenerator.Model
{
    class Student
    {
        /// <summary>
        /// ФИО
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ФИО в родительном падеже
        /// </summary>
        public string NameRP { get; set; }

        /// <summary>
        /// Направление
        /// </summary>
        public string StudyDirection { get; set; }

        /// <summary>
        /// Профиль
        /// </summary>
        public string StudyProfile { get; set; }

        /// <summary>
        /// Группа
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Программа обучения
        /// </summary>
        public string Programm { get; set; }
    }
}
