﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PresentationClock
{
    class ScreenPainter
    {
        [DllImport("User32.dll")]
        public static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("User32.dll")]
        public static extern void ReleaseDC(IntPtr hwnd, IntPtr dc);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool InvalidateRect(
            IntPtr hWnd,
            ref Rectangle lpRect,
            bool bErase);

        public Color OutlineForeColor { get; set; } = Color.White;
        public float OutlineWidth { get; set; } = 4;
        public ScreenPainter()
        {

        }

        public void DrawString(int x, int y, string str)
        {
            IntPtr desktopDC = GetDC(IntPtr.Zero);

            Graphics g = Graphics.FromHdc(desktopDC);

            using (GraphicsPath gp = new GraphicsPath())
            using (Pen outline = new Pen(OutlineForeColor, OutlineWidth)
                { LineJoin = LineJoin.Round })
            using (StringFormat sf = new StringFormat())
            using (Brush foreBrush = new SolidBrush(Color.Black))
            {
                gp.AddString(str, FontFamily.GenericMonospace, (int)FontStyle.Bold,
                    22, new Point(x,y), sf);
                g.SmoothingMode = SmoothingMode.None;
                g.DrawPath(outline, gp);
                g.FillPath(foreBrush, gp);
            }

            ReleaseDC(IntPtr.Zero, desktopDC);
        }

        public void DrawFrame()
        {
            IntPtr desktopDC = GetDC(IntPtr.Zero);

            Graphics g = Graphics.FromHdc(desktopDC);

            using (GraphicsPath gp = new GraphicsPath())
            using (Pen outline = new Pen(OutlineForeColor, OutlineWidth)
                { LineJoin = LineJoin.Round })
            using (StringFormat sf = new StringFormat())
            using (Brush foreBrush = new SolidBrush(Color.Black))
            {
                gp.AddRectangle(new Rectangle(0,0, (int)g.VisibleClipBounds.Width, (int)g.VisibleClipBounds.Height));
                g.SmoothingMode = SmoothingMode.None;
                g.DrawPath(outline, gp);
            }

            ReleaseDC(IntPtr.Zero, desktopDC);
        }
    }
}
