﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationClock.Configuration
{
    class ProcessSection : ConfigurationSection
    {
        [ConfigurationProperty("processes", IsDefaultCollection = true)]
        public ProcessCollection Processes
        {
            get { return (ProcessCollection)this["processes"]; }
            set { this["processes"] = value; }
        }
    }
}
