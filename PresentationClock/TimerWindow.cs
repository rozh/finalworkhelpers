﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using NAudio.Wave;
using PresentationClock.Configuration;
using Timer = System.Timers.Timer;

namespace PresentationClock
{
    public partial class TimerWindow : Form
    {
        private Point _mouseClickPoint;
        private readonly Timer _pollTimer;
        private bool _presentationStart;
        private readonly NotifyIcon _notifyIcon;
        private string _timeString = string.Empty;
        private readonly List<string> _processNames;
        private DateTime _startTime = DateTime.Now;
        private DateTime _endTime = DateTime.Now;
        private readonly ScreenPainter _sp;
        private bool _audioPlayed;

        private bool _useReverseTimer;
        private bool _useScreenSignal;
        private bool _useAudioSignal;
        private string _audioPath = string.Empty;
        private int _timeLimit;
        private Color _timeColor = Color.Black;

        public Color OutlineForeColor { get; set; } = Color.White;
        public float OutlineWidth { get; set; } = 6;

        public TimerWindow()
        {
            InitializeComponent();
            _mouseClickPoint = new Point(0,0);
            _notifyIcon = new NotifyIcon
            {
                Text = @"Presentation Timer",
                BalloonTipIcon = ToolTipIcon.Info,
                BalloonTipText = @"Таймер запущен",
                BalloonTipTitle = @"Presentation Timer",
                ContextMenu = new ContextMenu(new[]
                {
                    new MenuItem("Выход"),
                }),
                Icon = SystemIcons.Application
            };
            _notifyIcon.ContextMenu.MenuItems[0].Click += (sender, args) =>
            {
                _notifyIcon.Visible = false;
                Application.Exit();
            };

            _processNames = new List<string>();
            LoadProcessNames();
            LoadSettings();

            _sp = new ScreenPainter
            {
                OutlineWidth = 20,
                OutlineForeColor = Color.Red
            };

            _pollTimer = new Timer(TimeSpan.FromSeconds(1).TotalMilliseconds)
            {
                AutoReset = true,
            };
            _pollTimer.Elapsed += PollTimerOnElapsed;
            _pollTimer.Start();
        }

        private void LoadProcessNames()
        {
            var section =
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
                    .Sections["ProcessSection"] as ProcessSection;
            if (section != null)
                foreach (var process in section.Processes)
                {
                    _processNames.Add(((ProcessElement) process).Name);
                }
        }

        private void LoadSettings()
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            if (settings["UseReversCount"] != null)
            {
                _useReverseTimer = settings["UseReversCount"].Value == "1";
            }
            if (settings["UseScreenSignal"] != null)
            {
                _useScreenSignal = settings["UseScreenSignal"].Value == "1";
            }
            if (settings["UseAudioSignal"] != null)
            {
                _useAudioSignal = settings["UseAudioSignal"].Value == "1";
            }
            if (settings["TimeLimit"] != null)
            {
                int time;
                if (int.TryParse(settings["TimeLimit"].Value, NumberStyles.Any, CultureInfo.InvariantCulture, out time))
                    _timeLimit = time;
            }
            if (settings["AudioFile"] != null)
            {
                if (File.Exists(settings["AudioFile"].Value))
                    _audioPath = settings["AudioFile"].Value;
            }
        }

        private void PollTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            if (IsPresentationRunning())
            {
                if (!_presentationStart)
                {
                    _startTime = DateTime.Now;
                    _endTime = DateTime.Now + TimeSpan.FromSeconds(_timeLimit);
                    _presentationStart = true;
                }

                if (_timeLimit > 0)
                {
                    if (DateTime.Now > _endTime)
                    {
                        _timeColor = Color.Red;
                        if (_useScreenSignal) PaintFrame();
                        if (_useAudioSignal && !_audioPlayed)
                        {
                            _audioPlayed = true;
                            PlaySound();
                        }
                    }
                }

                ShowTime();
            }
            else
            {
                if (_presentationStart)
                {
                    _startTime = DateTime.Now;
                    _endTime = DateTime.Now + TimeSpan.FromSeconds(_timeLimit);
                    _presentationStart = false;
                    _audioPlayed = false;
                    _timeColor = Color.Black;
                    ShowTime();
                }
            }
        }

        private void ShowTime()
        {
            _timeString =
                (_useReverseTimer ? (_endTime - DateTime.Now) : (DateTime.Now - _startTime)).ToString(@"mm\:ss");

            if (InvokeRequired)
            {
                Invoke(new Action(Invalidate));
            }
            else
            {
                Invalidate();
            }
        }

        private void PlaySound()
        {
            try
            {
                using (var audioFile = new AudioFileReader(_audioPath))
                using (var outputDevice = new WaveOutEvent())
                {
                    outputDevice.Init(audioFile);
                    outputDevice.Play();
                    while (outputDevice.PlaybackState == PlaybackState.Playing)
                    {
                        Thread.Sleep(10);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void PaintFrame()
        {
            _sp.DrawFrame();
        }

        private void TimerWindow_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Cursor = Cursors.SizeAll;
                _mouseClickPoint.X = e.X;
                _mouseClickPoint.Y = e.Y;
            }
        }

        private void TimerWindow_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Location = new Point(Location.X + e.X - _mouseClickPoint.X, Location.Y + e.Y - _mouseClickPoint.Y);
            }
        }

        private void TimerWindow_MouseUp(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                Cursor = Cursors.Default;
            }
        }
        private bool IsPresentationRunning()
        {
            Process[] prozesse = Process.GetProcesses();
            foreach (Process p in prozesse)
            {
                //searches for a running PowerPoint process
                if (p.ProcessName == "POWERPNT")
                {
                    return true;
                }

                if (_processNames.Any(pn => p.ProcessName.ToLowerInvariant().Contains(pn.ToLowerInvariant())))
                    return true;
            }
            return false;
        }

        private void TimerWindow_Load(object sender, EventArgs e)
        {
            _notifyIcon.ShowBalloonTip(1000);
            _notifyIcon.Visible = true;
            ShowTime();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(BackColor), ClientRectangle);
            using (GraphicsPath gp = new GraphicsPath())
            using (Pen outline = new Pen(OutlineForeColor, OutlineWidth)
                { LineJoin = LineJoin.Round })
            using (StringFormat sf = new StringFormat())
            using (Brush foreBrush = new SolidBrush(_timeColor))
            {
                gp.AddString(_timeString, FontFamily.GenericMonospace, (int)FontStyle.Bold,
                    22, ClientRectangle, sf);
                e.Graphics.SmoothingMode = SmoothingMode.None;
                e.Graphics.DrawPath(outline, gp);
                e.Graphics.FillPath(foreBrush, gp);
            }
        }
    }
}
