﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace CDCopier
{
    class Program
    {
        static string _toPath;
        static string _fromPath;
        private static readonly Dictionary<string, string> TranslitDictionary = new Dictionary<string, string>();
        private static Regex _regex;

        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Укажите откуда и куда копировать данные:");
                Console.WriteLine(AppDomain.CurrentDomain.FriendlyName + @" D: C:\tmp");
                Console.WriteLine();

                Console.WriteLine("Можно использовать автоматическое определение диска CD-ROM:");
                Console.WriteLine(AppDomain.CurrentDomain.FriendlyName + @" auto C:\tmp");
                return;
            }

            _toPath = args[1];
            _regex = new Regex("\\d{2}_\\d{4}");
            InitTransliteration();

            if (args[0] == "auto")
            {
                var dw = new DriveWatcher();
                dw.OpticalDiskArrived += DwOnOpticalDiskArrived;
                dw.Start();
                Console.ReadKey();
            }
            else
            {
                _fromPath = args[0];
                CopyDisk();
            }

        }

        private static void DwOnOpticalDiskArrived(object sender, OpticalDiskArrivedEventArgs opticalDiskArrivedEventArgs)
        {
            if (opticalDiskArrivedEventArgs.Drive.IsReady)
            {
                Console.WriteLine("Вставлен диск");
                CopyDisk(opticalDiskArrivedEventArgs.Drive.Name);

                try
                {
                    EjectMedia.Eject(opticalDiskArrivedEventArgs.Drive.Name);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private static void CopyDisk()
        {
            CopyDisk(_fromPath);
        }

        private static void CopyDisk(string disk)
        {
            TreeScan(disk);
            Console.WriteLine("Копирование завершено");
        }

        private static void TreeScan(string sDir)
        {
            Console.WriteLine($"Директория: {sDir}");
            foreach (string file in Directory.GetFiles(sDir,"*.pdf"))
            {
                var fileName = Path.GetFileName(file);
                if (_regex.IsMatch(fileName))
                {
                    var savePath = _toPath + Path.DirectorySeparatorChar + NormalizeName(fileName);
                    Console.Write($"\tКопирование: {file} -> {savePath} ... ");
                    File.Copy(file, savePath, true);
                    Console.WriteLine("готово");
                }
                else
                {
                    Console.WriteLine($"\tПропуск {file}");
                }
            }
            foreach (string d in Directory.GetDirectories(sDir))
            {
                TreeScan(d);
            }
        }

        private static string NormalizeName(string name)
        {
            return Translit(name);
        }
        
        public static string Translit(string text)
        {
            string output = text;
            
            Dictionary<string, string> tdict = TranslitDictionary;

            foreach (KeyValuePair<string, string> key in tdict)
            {
                output = output.Replace(key.Key, key.Value);
            }
            return output;
        }
        
        private static void InitTransliteration()
        {
            TranslitDictionary.Add("Є", "EH");
            TranslitDictionary.Add("І", "I");
            TranslitDictionary.Add("і", "i");
            TranslitDictionary.Add("№", "#");
            TranslitDictionary.Add("є", "eh");
            TranslitDictionary.Add("А", "A");
            TranslitDictionary.Add("Б", "B");
            TranslitDictionary.Add("В", "V");
            TranslitDictionary.Add("Г", "G");
            TranslitDictionary.Add("Д", "D");
            TranslitDictionary.Add("Е", "E");
            TranslitDictionary.Add("Ё", "JO");
            TranslitDictionary.Add("Ж", "ZH");
            TranslitDictionary.Add("З", "Z");
            TranslitDictionary.Add("И", "I");
            TranslitDictionary.Add("Й", "JJ");
            TranslitDictionary.Add("К", "K");
            TranslitDictionary.Add("Л", "L");
            TranslitDictionary.Add("М", "M");
            TranslitDictionary.Add("Н", "N");
            TranslitDictionary.Add("О", "O");
            TranslitDictionary.Add("П", "P");
            TranslitDictionary.Add("Р", "R");
            TranslitDictionary.Add("С", "S");
            TranslitDictionary.Add("Т", "T");
            TranslitDictionary.Add("У", "U");
            TranslitDictionary.Add("Ф", "F");
            TranslitDictionary.Add("Х", "KH");
            TranslitDictionary.Add("Ц", "C");
            TranslitDictionary.Add("Ч", "CH");
            TranslitDictionary.Add("Ш", "SH");
            TranslitDictionary.Add("Щ", "SHH");
            TranslitDictionary.Add("Ъ", "'");
            TranslitDictionary.Add("Ы", "Y");
            TranslitDictionary.Add("Ь", "");
            TranslitDictionary.Add("Э", "EH");
            TranslitDictionary.Add("Ю", "YU");
            TranslitDictionary.Add("Я", "YA");
            TranslitDictionary.Add("а", "a");
            TranslitDictionary.Add("б", "b");
            TranslitDictionary.Add("в", "v");
            TranslitDictionary.Add("г", "g");
            TranslitDictionary.Add("д", "d");
            TranslitDictionary.Add("е", "e");
            TranslitDictionary.Add("ё", "jo");
            TranslitDictionary.Add("ж", "zh");
            TranslitDictionary.Add("з", "z");
            TranslitDictionary.Add("и", "i");
            TranslitDictionary.Add("й", "jj");
            TranslitDictionary.Add("к", "k");
            TranslitDictionary.Add("л", "l");
            TranslitDictionary.Add("м", "m");
            TranslitDictionary.Add("н", "n");
            TranslitDictionary.Add("о", "o");
            TranslitDictionary.Add("п", "p");
            TranslitDictionary.Add("р", "r");
            TranslitDictionary.Add("с", "s");
            TranslitDictionary.Add("т", "t");
            TranslitDictionary.Add("у", "u");

            TranslitDictionary.Add("ф", "f");
            TranslitDictionary.Add("х", "kh");
            TranslitDictionary.Add("ц", "c");
            TranslitDictionary.Add("ч", "ch");
            TranslitDictionary.Add("ш", "sh");
            TranslitDictionary.Add("щ", "shh");
            TranslitDictionary.Add("ъ", "");
            TranslitDictionary.Add("ы", "y");
            TranslitDictionary.Add("ь", "");
            TranslitDictionary.Add("э", "eh");
            TranslitDictionary.Add("ю", "yu");
            TranslitDictionary.Add("я", "ya");
            TranslitDictionary.Add("«", "");
            TranslitDictionary.Add("»", "");
            TranslitDictionary.Add("—", "-");
            TranslitDictionary.Add(" ", "_");
        }

    }
}
